describe("Upload Tests", () => {
    it("upload file and assert file name", () => {
        cy.visit("https://cmi.medisource.com/")

        cy.get('#mui-3').type('adesna'); //dito ang pag type ng username
        cy.get('#mui-4').type('Tester2022!'); //dito ang pag type ng password
        cy.get('.flex > .MuiButton-root').click();
        cy.get('.flex-1 > .MuiButtonBase-root > .material-icons').click();
        cy.get('.MuiList-root > :nth-child(2) > :nth-child(1) > .MuiButtonBase-root').click();
        cy.get('[href="/apps/inventory/item/new"] > .MuiListItemText-root > .MuiTypography-root').click();

        // upload file to the input field
        cy.get('input[type="file"]')
        .attachFile('Gojo.png')
    });
});