describe('New Stocktakes', () => {

  it('Tests New Stocktakes', function() 
  {
    cy.viewport(1920, 1080);//setting your windows size
      //visit page  
      cy.visit('https://cmi.medisource.com/login');     
      //dito ang pag type ng username     
      cy.get('#mui-1').type('adesna'); 
      //dito ang pag type ng password
      cy.get('#mui-2').type('Tester2022!'); 
      // log in button
      cy.get('.flex > .MuiButton-root').click();
      // click yung inventory
      cy.get('.MuiList-root > :nth-child(2) > :nth-child(1)').click();
      // click yung stocktaking
      cy.get('.MuiCollapse-entered > :nth-child(1) > :nth-child(1) > .muiltr-lwoibc > .muiltr-18k6fpl-MuiButtonBase-root-MuiListItem-root > .MuiListItemText-root > .MuiTypography-root').click();
      // click yung new stocktakes
      cy.get('[href="/apps/inventory/stocktaking/stocktake/new"] > .MuiListItemText-root > .MuiTypography-root').click();
      // click category search bar
      cy.get('#mui-9').click();
      // click yung first sa selection ng category
      cy.get('#mui-9-option-0').click();
      // click item search bar and enter inputs
      cy.get('#mui-11').type('Logi');
      // click yung "Logitech G Pro X Supelight"
      cy.get('#mui-11-option-0').click();
      // click the add button
      cy.get('.flex > div > .MuiButton-root' ).click();
      // click and enter an actual quantity
      cy.get('#a9ae427e-3a9c-44c2-8b18-83725245e2c9').type('2');
      // enter stock date
      cy.get('#stockDate').type('12/01/2022');
      // enter a comment
      cy.get('#comment').type('test');
      // click complete stocktake
      cy.get('.muiltr-t1j70 > div > .MuiButton-contained').click();
    }) 
  })