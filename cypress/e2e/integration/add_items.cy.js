describe('Add Items', () => {

  it('Test Add Items', function() 
  {
    cy.viewport(1920, 1080);//setting your windows size
      //visit page  
      cy.visit('https://cmi.medisource.com/login');     
      //dito ang pag type ng username     
      cy.get('#mui-1').type('adesna'); 
      //dito ang pag type ng password
      cy.get('#mui-2').type('Tester2022!'); 
      // log in button
      cy.get('.flex > .MuiButton-root').click();
      // click yung inventory
      cy.get('.MuiList-root > :nth-child(2) > :nth-child(1)').click();
      // click yung add items
      cy.get(':nth-child(1) > :nth-child(1) > [href="/apps/inventory/item/new"]').click();
      // click and enter the code
      cy.get('#code').type('walanakami456');
      // click yung type
      cy.get('#type-select').click();
      // dito yung pag-click sa code field
      cy.get('#menu-type > .MuiPaper-root > .MuiList-root').click();
      // dito yung mag input siya ng barcode
      cy.get('#barcode').type('01010101');
      // dito yung mag click siya ng category drop down
      cy.get('#category-select').click();
      // dito yung magselect siya ng BOOKSTORE
      cy.get('#menu-category > .MuiPaper-root > .MuiList-root').click();
      // dito yung mag click siya ng department drop down
      cy.get('#department-select').click();
      // dito yung magselect siya ng DEPARTMENT
      cy.get('#menu-department > .MuiPaper-root > .MuiList-root').click();
      // dito yung mag input siya ng NAME
      cy.get('#name').type('Adesna');
      // dito yung mag input siya ng DESCRIPTION
      cy.get('#description').type('Books');
      // dito yung mag input siya ng COST
      cy.get('#cost').type('25');
      // dito yung mag input siya ng SRP
      cy.get('#srp').type('20');
      // dito yung when you clicked the supplier dropdown field
      cy.get('#supplier-select').click();
      // dito yung kusa siya magse-select ng supplier
      cy.get('#menu-supplier > .MuiPaper-root > .MuiList-root > [tabindex="0"]').click();
      // click and enter tags
      cy.get('.rti--input').type('123456');
      // select course
      cy.get('#course-select').click();
      // select a course
      cy.get('#menu-course > .MuiPaper-root > .MuiList-root').click();
      // select and enter location
      cy.get('#location').type('Puso mo');
      // select and enter an author
      cy.get('#author').type('christian adesna');
      // select publisher
      cy.get('#publisher-select').click();
      // choose a publisher
      cy.get('#menu-publisher > .MuiPaper-root > .MuiList-root > [tabindex="0"]').click();
      // click and enter inputs on ISBN
      cy.get('#isbn').type('test');
      // Click and enter inputs on count quantity
      cy.get('#countQuantity').type('20');


      // upload file to the input field
      cy.get('input[type="file"]')
      .attachFile('Gojo.png')

      // click save button
      cy.get('.muiltr-3pcfmk > .flex > :nth-child(2)').click();



  }) 
})
