describe('New Postings', () => {

  it('Tests New Postings', function() 
  {
    cy.viewport(1920, 1080);//setting your windows size
      //visit page  
      cy.visit('https://cmi.medisource.com/login');     
      //dito ang pag type ng username     
      cy.get('#mui-1').type('adesna'); 
      //dito ang pag type ng password
      cy.get('#mui-2').type('Tester2022!'); 
      // log in button
      cy.get('.flex > .MuiButton-root').click();
      // click yung inventory
      cy.get('.MuiList-root > :nth-child(2) > :nth-child(1)').click();
      // click new postings
      cy.get('[href="/apps/inventory/posting/new"]').click();
      // click and enter input on the textbox
      cy.get('#mui-9').type('Battery Energizer Max AAA');
      // click the searched item
      cy.get('#mui-9-option-0').click();
      // click quantity textbox and enter inputs
      cy.get('#quantity').type('1');
      // click add
      cy.get(':nth-child(2) > .p-14 > .MuiButton-root').click();
      // click date
      cy.get('#date').type('10/13/2022');
      // click save
      cy.get('.muiltr-t1j70 > div').click();
      // click new item
      cy.get(':nth-child(3) > .p-14 > .MuiButton-root').click();
      // click random code
      cy.get('.muiltr-19zkrbt > .flex > .MuiButton-root').click();
      // click type
      cy.get('#type-select').click();
      // select from the dropdown
      cy.get('#menu-type > .MuiPaper-root > .MuiList-root').click();
      // click barcode and enter inputs
      cy.get('#barcode').type('0000011111');
      // click category
      cy.get('#category-select').click();
      // select a category on the dropdown
      cy.get('#menu-category > .MuiPaper-root > .MuiList-root').click();
      // click department
      cy.get('#department-select').click();
      // select a department from the dropdown
      cy.get('#menu-department > .MuiPaper-root > .MuiList-root').click();
      // click name and enter inputs
      cy.get('#name').type('Galileo Galilei');
      // click description and enter inputs
      cy.get('#description').type('test');
      // click cost and enter inputs
      cy.get('#cost').type('10');
      // click srp and enter inputs
      cy.get('#srp').type('15');
      // click suppliers
      cy.get('#supplier-select').click();
      // select a supplier from the dropdown
      cy.get('#menu-supplier > .MuiPaper-root > .MuiList-root').click();

            // upload file to the input field
            cy.get('input[type="file"]')
            .attachFile('Feedback.jpg')

      // click save
      cy.get('.px-16 > .MuiButton-containedPrimary').click();

      //Ito yung part na direct siyang mag save na walang nilalagay na inputs dun sa product
      
      // click save button
      cy.get('.muiltr-t1j70 > div > .MuiButton-contained').click();

      // Ito naman yung part na mag search ka ng code/product at maglalagay ng quantity pero hindi ka magse-save. Instead ay mag click ka ng ibang module

      // click code/product at mag input
      cy.get('#mui-9').type('Battery Energizer Max AAA');
      // click the searched item
      cy.get('#mui-9-option-0').click();
      // click quantity textbox and enter inputs
      cy.get('#quantity').type('1');
      // mag click ng ibang module papuntang postings
      cy.get('.MuiCollapse-entered > :nth-child(1) > :nth-child(1) > [href="/apps/inventory/postings"]').click();
      // I-select neto yung "YES" sa pop-up validation
      cy.get(':nth-child(1) > .swal-button').click();



    }) 
  })
  