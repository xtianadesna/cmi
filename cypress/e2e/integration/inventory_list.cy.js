describe('Inventory List', () => {

  it('Test Inventory List', function() 
  {
    
      cy.viewport(1920, 1080);//setting your windows size
      //visit page  
      cy.visit('https://cmi.medisource.com/login');     
      //dito ang pag type ng username     
      cy.get('#mui-1').type('adesna'); 
      //dito ang pag type ng password
      cy.get('#mui-2').type('Tester2022!'); 
      // log in button
      cy.get('.flex > .MuiButton-root').click();
      // click yung Inventory
      cy.get('.muiltr-t8gd1p > .MuiList-root > :nth-child(2) > :nth-child(1)').click();
      // click yung Inventory List
      cy.get('[href="/apps/inventory/items"]').click();
      // click yung edit button
      cy.get(':nth-child(1) > :nth-child(13) > .MuiButtonBase-root').click();
      // click yung auto code button
      cy.get(':nth-child(2) > .items-center > .muiltr-19zkrbt > .flex > .bg-icon > .MuiButtonBase-root').click();
      // click yung type
      cy.get('#type-select').click();
      // mamili ng bagong type
      cy.get('#menu-type > .MuiPaper-root > .MuiList-root').click();
      // click save
      cy.get('.muiltr-3pcfmk > .flex > :nth-child(2)').click();




  })
})