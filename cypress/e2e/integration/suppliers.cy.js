describe('Suppliers', () => {

  it('Tests Suppliers', function() 
  {
    cy.viewport(1920, 1080);//setting your windows size
      //visit page  
      cy.visit('https://cmi.medisource.com/login');     
      //dito ang pag type ng username     
      cy.get('#mui-1').type('adesna'); 
      //dito ang pag type ng password
      cy.get('#mui-2').type('Tester2022!'); 
      // log in button
      cy.get('.flex > .MuiButton-root').click();
      // click yung inventory
      cy.get('.MuiList-root > :nth-child(2) > :nth-child(1)').click();
      // click suppliers
      cy.get('[href="/apps/inventory/suppliers"]').click();
      // click "New Suppliers" button
      cy.get('.muiltr-19zkrbt > .MuiButton-root').click();
      // Enter a name
      cy.get('#name').type('Adesna');
      // Enter a contact person
      cy.get('#contactPerson').type('Daryl');
      // Enter address
      cy.get('#address').type('San Vicente, Urdaneta City');
      // Enter contact number
      cy.get('#contact').type('9566143608');
      // click save button
      cy.get('#mui-8').click();
      
      // Search yung newly added supplier
      cy.get('.justify-start > .MuiPaper-root').type('Adesna');
      // edit a supplier
      cy.get(':nth-child(1) > .MuiTableCell-alignRight > .muiltr-19zkrbt > .MuiButtonBase-root').click();
      // clear data in contact person
      //cy.get('#contactPerson').clear('Daryl');
      // edit contact person
      //cy.get('#contactPerson').type('Daryl Espanto');
      // Save
      //cy.get('#mui-23').click();

  })
})