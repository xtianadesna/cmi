describe('Postings', () => {

  it('Test Postings', function() 
  {
    cy.viewport(1920, 1080);//setting your windows size
      //visit page  
      cy.visit('https://cmi.medisource.com/login');     
      //dito ang pag type ng username     
      cy.get('#mui-1').type('adesna'); 
      //dito ang pag type ng password
      cy.get('#mui-2').type('Tester2022!'); 
      // log in button
      cy.get('.flex > .MuiButton-root').click();
      // click yung inventory
      cy.get('.MuiList-root > :nth-child(2) > :nth-child(1)').click();
      // click postings
      cy.get('[href="/apps/inventory/postings"]').click();
      // click edit button
      cy.get(':nth-child(1) > .MuiTableCell-alignCenter > .MuiButtonBase-root').click();
      // click the edit button sa may actions
      cy.get('.MuiTableCell-alignCenter > :nth-child(1) > .MuiButtonBase-root').click();
      // click the plus sign
      cy.get('.w-full > .flex > :nth-child(3)').click();
      // click save
      cy.get('.px-16 > .MuiButton-containedPrimary').click();
      // click delete button
      cy.get('[aria-label="Delete undefined"] > .material-icons').click();

      //Punta sa print view

      // click return button
      cy.get('[style="opacity: 1; transform: none;"] > .MuiTypography-root').click();
      // click print button
      cy.get('.MuiButton-containedSecondary').click();
      

      // Filter By: From-to-To

      // click filter from
      //cy.get(':nth-child(3) > .MuiOutlinedInput-root > .MuiInputAdornment-root > .MuiButtonBase-root > [data-testid="CalendarIcon"]').click();
      // magclick ng date
      //cy.get('.muiltr-i6bazn > :nth-child(2) > :nth-child(1) > .MuiButtonBase-root').click();
      
      // click from text box and enter date
      cy.get('#dataFrom').type('10/02/2022');
      //cy.wait('get ')
      // click to text box and enter date
      cy.get('#dateTo').type('10/10/2022');


      // Ito yung part na mag create ng new postings
      // click new postings
      cy.get('.muiltr-z8zpr7 > div > .MuiButton-root > .hidden').click();
      // click and enter input on the textbox
      cy.get('#mui-62').type('Battery Energizer Max AAA');
      // click the searched item
      cy.get('#mui-62-option-0').click();
      // click quantity textbox and enter inputs
      cy.get('#quantity').type('1');
      // click add
      cy.get(':nth-child(2) > .p-14 > .MuiButton-root').click();
      // click date
      cy.get('#date').type('10/13/2022');
      // click save
      cy.get('.muiltr-t1j70 > div').click();
      // click new item
      cy.get(':nth-child(3) > .p-14 > .MuiButton-root').click();
      // click random code
      cy.get('.muiltr-19zkrbt > .flex > .MuiButton-root').click();
      // click type
      cy.get('#type-select').click();
      // select from the dropdown
      cy.get('#menu-type > .MuiPaper-root > .MuiList-root').click();
      // click barcode and enter inputs
      cy.get('#barcode').type('0000011111');
      // click category
      cy.get('#category-select').click();
      // select a category on the dropdown
      cy.get('#menu-category > .MuiPaper-root > .MuiList-root').click();
      // click department
      cy.get('#department-select').click();
      // select a department from the dropdown
      cy.get('#menu-department > .MuiPaper-root > .MuiList-root').click();
      // click name and enter inputs
      cy.get('#name').type('Galileo Galilei');
      // click description and enter inputs
      cy.get('#description').type('test');
      // click cost and enter inputs
      cy.get('#cost').type('10');
      // click srp and enter inputs
      cy.get('#srp').type('15');
      // click suppliers
      cy.get('#supplier-select').click();
      // select a supplier from the dropdown
      cy.get('#menu-supplier > .MuiPaper-root > .MuiList-root').click();

            // upload file to the input field
            cy.get('input[type="file"]')
            .attachFile('Feedback.jpg')

      // click save
      cy.get('.px-16 > .MuiButton-containedPrimary').click();


    }) 
  })
  