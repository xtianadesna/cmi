describe('Login Page', () => {

    it('Login the Account', function() 
    {
        //visit page  
        cy.visit('https://cmi.medisource.com/');     
        //dito ang pag type ng username     
        cy.get('#mui-3').type('adesna'); 
        //dito ang pag type ng password
        cy.get('#mui-4').type('Tester2022!'); 
        // log in button
        cy.get('.flex > .MuiButton-root').click();
        // click yung navi button
        cy.get('.flex-1 > .MuiButtonBase-root > .material-icons').click();
        // click yung 
        cy.get('.MuiList-root > :nth-child(2) > :nth-child(1) > .MuiButtonBase-root').click();
        cy.get('[href="/apps/inventory/item/new"] > .MuiListItemText-root > .MuiTypography-root').click();
        cy.get(':nth-child(1) > .MuiFormGroup-root > :nth-child(1) > .MuiTypography-root').click();
        cy.get('#code').type('hindikamahal123');
        cy.get('#type-select').click();
        // dito yung pag-click sa code field
        cy.get('#menu-type > div.MuiPaper-root.MuiPaper-elevation.MuiPaper-rounded.MuiPaper-elevation1.MuiMenu-paper.MuiPaper-root.MuiPaper-elevation.MuiPaper-rounded.MuiPaper-elevation8.MuiPopover-paper.muiltr-1mgczf1-MuiPaper-root-MuiMenu-paper-MuiPaper-root-MuiPopover-paper > ul > li:nth-child(1)').click();
        cy.get('#barcode').type('01010101');
        cy.get('#category-select').click();
        cy.get('[data-value="Bookstore"]').click();
        cy.get('#department-select').click();
        cy.get('[data-value="School and Office Supply Department"]').click();
        cy.get('#name').type('Adesna');
        cy.get('#description').type('Books');
        cy.get('#cost').type('25');
        cy.get('#srp').type('20');
        cy.get('#supplier-select').click();
        cy.get('[data-value="Supplier 1 "]').click();

        // upload file to the input field
        cy.get('input[type="file"]')
        .attachFile('Gojo.png')
        
    }) 
})